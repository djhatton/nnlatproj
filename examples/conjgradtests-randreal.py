import matplotlib
matplotlib.use('Agg')
import numpy as np
import pickle
from scipy.sparse.linalg import *
import sys
import matplotlib.pyplot as plt
sys.path.insert(1,'../')
from latmattools.manip import manip
from latmattools.algs import conjgrad
import random

Ls = 2
Lt = 2

source = np.ones(3*Lt*Ls**3,dtype=np.cdouble)
#source[0] = 1
#source[1] = 1
#source[2] = 1

preds = pickle.load(open('./Predictions/invMatPredict_realrand.p','rb'))


np.random.seed(0)
matArray = pickle.load(open('./picklejar/realrandmats-10000', 'rb'))
invMatArray = pickle.load(open('./picklejar/realrandmats-10000', 'rb'))
matDict = {'matArray': matArray
            , 'invMatArray': invMatArray
           }
trainSplit = 0.8  # zSpace['TrainSplit']

boolMask = np.array([True if np.random.uniform() < trainSplit else False
                     for _ in range(matDict['matArray'].shape[0])
                     ])
mats = matDict['matArray'][np.logical_not(boolMask)]
invmats = matDict['invMatArray'][np.logical_not(boolMask)]

print(invmats[0])
print()
print(preds[0])
print()

print(np.shape(mats))

#mats = pickle.load(open('./picklejar/allmats-10000','rb'))[-1972:]

basicitcounts = []
preditcounts = []
basicdists = []
preddists = []

for cfg in range(1972):

    matpred = preds[cfg]
    x0 = matpred.dot(source)
    mat = mats[cfg]

    CG = conjgrad.ConjGrad(48,source)
    CG.setA(mat)
    CG()

    biters = CG.niters
    bdist = CG.initdist

    CG.setx0(x0)
    CG()

    piters = CG.niters
    pdist = CG.initdist

    basicitcounts.append(biters)
    preditcounts.append(piters)

    basicdists.append(bdist)
    preddists.append(pdist)



print(np.sum(basicitcounts))
print(np.sum(preditcounts))
print()
print(np.mean(basicitcounts))
print(np.mean(preditcounts))
print()
print(np.std(basicitcounts))
print(np.std(preditcounts))

#print()
#print(basicdists)
#print()
#print(preddists)

#plt.plot(range(1972),basicitcounts,color='r',marker='x')
#plt.plot(range(1972),preditcounts,color='b',marker='x')
plt.plot(range(1972),[p1-p2 for p1,p2 in zip(basicitcounts,preditcounts)],color='b')
plt.savefig('./cgtests_diff_realrand.pdf')
plt.close()

plt.plot(range(1972),[np.sqrt(p) for p in preddists],color='b')
plt.plot(range(1972),[np.sqrt(p) for p in basicdists],color='r')
plt.savefig('./cgtests_distances_realrand.pdf')
plt.close()
