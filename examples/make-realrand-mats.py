import numpy as np
from scipy.sparse import random
from scipy import stats
import sys
import pickle

np.set_printoptions(precision=2, threshold=sys.maxsize)

np.random.seed(0)

diag = 0.01*np.identity(48)

mats = np.zeros((10000,48,48))
invmats = np.zeros((10000,48,48))

rvs = stats.norm(0,0.5).rvs

for cfg in range(0,10000):

    mat = random(48,48,density=0.1,data_rvs=rvs)

    mats[cfg] = np.add(mat.A,diag)
    #print(np.linalg.det(mat.A))
    inmat = np.linalg.inv(np.add(mat.A,diag))
    maxval = np.amax(np.absolute(inmat)) 
    invmats[cfg] = inmat/np.absolute(maxval)

pickle.dump(mats,open('./picklejar/realrandmats-10000','wb'))
pickle.dump(invmats,open('./picklejar/realrandinvmats-10000','wb'))
