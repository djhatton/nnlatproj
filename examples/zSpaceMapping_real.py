import numpy as np
import pickle
from pprint import pprint as pp
# import tensorflow as tf
# from tensorflow import keras
# from tensorflow.keras import layers
# from tensorflow.keras import regularizers
# from tensorflow.keras import initializers


def initArch(nbDimms, actFct, nbNeur, λVal=0.0, fitHandle='ZSpaceFit_realrand'):
    '''
    '''
    import tensorflow as tf
    from tensorflow import keras
    from tensorflow.keras import layers
    from tensorflow.keras import regularizers
    from tensorflow.keras import initializers

    # Initialise the Neural network
    inputLayer = keras.Input(shape=(nbDimms,))
    denseLayer = layers.Dense(nbNeur, activation=actFct, activity_regularizer=regularizers.l2(λVal))
    a1 = denseLayer(inputLayer)

    outLayer = layers.Dense(2, activation=actFct, activity_regularizer=regularizers.l2(λVal))(a1)
    model = keras.Model(inputs=inputLayer, outputs=outLayer, name=fitHandle)

    #  Plot out the neural net and compile it
    #keras.utils.plot_model(model, 'FitModel/' + fitHandle + ".png", show_shapes=True)
    model.summary()
    model.compile(loss='mean_squared_error', optimizer='adam')

    return model


if __name__ == '__main__':
    #  Load up Z spaces
    with open('./LatentZspaces/matArray_Zspace_linArch_N160_relu_tanh_Z2_realrand.p', 'rb') as pZIn:
        zSpace_Dict = pickle.load(pZIn)
    with open('./LatentZspaces/invMatArray_Zspace_linArch_N500_relu_tanh_Z2_realrand.p', 'rb') as pZIn:
        zSpaceInv_Dict = pickle.load(pZIn)
    zSpace, zSpaceInv = zSpace_Dict['Zspace'], zSpaceInv_Dict['Zspace']

    # # Set the random seed
    # np.random.seed(0)
    # matArray = pickle.load(open('./picklejar/allmats-10000', 'rb'))
    # matDict = {'matArray': matArray
    #            # , 'invMatArray': invMatArray
    #            }
    # trainSplit = 0.8
    #
    # boolMask = np.array([True if np.random.uniform() < trainSplit else False
    #                      for _ in range(matDict['matArray'].shape[0])
    #                      ])
    #
    # trainArray, testArray = matDict['matArray'][boolMask], matDict['matArray'][np.logical_not(boolMask)]

    # Initialise the fitting neural net.
    fitFct = 'relu'
    nbNeur = 100
    mBatch = 128
    nbEpochs = 1000
    fitModel = initArch(zSpace.shape[1], fitFct, nbNeur)

    # Fit the stuff.
    fitHistory = fitModel.fit(zSpace, zSpaceInv, batch_size=mBatch, epochs=nbEpochs, validation_split=0.2, verbose=1)
    fitModel.save('FitModel/latZfit_randreal')
    # fitModel.predict()
