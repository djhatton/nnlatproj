import numpy as np
import pickle
from pprint import pprint as pp

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras import initializers


if __name__ == '__main__':
    #  Load up Z spaces
    with open('./LatentZspaces/matArray_Zspace_linArch_N160_relu_tanh_Z2.p', 'rb') as pZIn:
        zSpace_Dict = pickle.load(pZIn)
    zSpace = zSpace_Dict['Zspace']

    # Set the random seed and split the matrix set
    np.random.seed(0)
    matArray = pickle.load(open('./picklejar/allmats-10000', 'rb'))
    matDict = {'matArray': matArray
               # , 'invMatArray': invMatArray
               }
    trainSplit = 0.8  # zSpace['TrainSplit']
    nMatSize = 48
    boolMask = np.array([True if np.random.uniform() < trainSplit else False
                         for _ in range(matDict['matArray'].shape[0])
                         ])
    testArray = matDict['matArray'][np.logical_not(boolMask)]

    # Flatten the matrices for the linear architecture
    nbMatrices = testArray.shape[0]
    testMats = []
    for i in range(nbMatrices):
        testMats.append(testArray[i].flatten())
    testMats = np.array(testMats)

    # Encoder from the matrix VAE
    matEncoder = keras.models.load_model('VAEs/linArch_N160_relu_tanh_Z2/Encoder')
    zMSpace, _, _ = matEncoder.predict(testMats)

    # Mapping between latent spaces
    zfitNN = keras.models.load_model('FitModel/latZfit')
    zMinvSpace = zfitNN.predict(zMSpace)

    # Decoder from the inverse matrix VAE
    invMatDecoder = keras.models.load_model('VAEs/linArch_N500_relu_tanh_Z2/Decoder')
    predInvMats_flat = invMatDecoder.predict(zMinvSpace)

    # print(zMSpace[0], zMinvSpace[0])
    # print(f'{testMats.shape}->{zMSpace.shape}->{zMinvSpace.shape}->{predInvMats_flat.shape}')

    predInvMats = []
    for i in range(predInvMats_flat.shape[0]):
        predInvMats.append(predInvMats_flat[i].reshape(nMatSize, nMatSize))
    predInvMats = np.array(predInvMats)

    with open('Predictions/invMatPredict.p', 'wb') as pcklOut:
        pickle.dump(predInvMats, pcklOut)
    # invMatDecoder = keras.models.load_model('VAEs/linArch_N500_relu_tanh_Z2/Decoder')
