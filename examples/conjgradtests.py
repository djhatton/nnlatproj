import matplotlib
matplotlib.use('Agg')
import numpy as np
import pickle
from scipy.sparse.linalg import *
import sys
import matplotlib.pyplot as plt
sys.path.insert(1,'../')
from latmattools.manip import manip
from latmattools.algs import conjgrad
import random

Ls = 2
Lt = 2

# source vector setup
source = np.ones(3*Lt*Ls**3,dtype=np.cdouble)

# load predicted inverses
preds = pickle.load(open('./Predictions/invMatPredict.p','rb'))

# apply random mask for validation set based on what was used for training set
np.random.seed(0)
matArray = pickle.load(open('./picklejar/allmats-10000', 'rb'))
invMatArray = pickle.load(open('./picklejar/allinvmats-10000', 'rb'))
matDict = {'matArray': matArray
            , 'invMatArray': invMatArray
           }
trainSplit = 0.8  # zSpace['TrainSplit']

boolMask = np.array([True if np.random.uniform() < trainSplit else False
                     for _ in range(matDict['matArray'].shape[0])
                     ])
mats = matDict['matArray'][np.logical_not(boolMask)]
invmats = matDict['invMatArray'][np.logical_not(boolMask)]

cgtest = conjgrad.CGTest(preds,mats,source)
cgtest()
cgtest.make_plots()

