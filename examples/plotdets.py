import matplotlib
matplotlib.use('Agg')
import numpy as np
import pickle
import matplotlib.pyplot as plt

matArray = pickle.load(open('./picklejar/allmats-10000', 'rb'))

dets = [np.linalg.norm(np.linalg.det(p)) for p in matArray]

plt.hist(dets,range=(0,1000),bins=100)
plt.savefig('determinants_hist_mag.pdf')
plt.close()

