import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import pickle
import sys
sys.path.insert(1,'../')
from latmattools.manip import manip
np.set_printoptions(precision=2,threshold=sys.maxsize)

model = keras.Sequential()

Ls = 2
Lt = 2

fmsize = 2*(3*Lt*Ls**3)**2

mats = pickle.load(open('./picklejar/allmats-10000','rb'))
invmats = pickle.load(open('./picklejar/allinvmats-10000','rb'))


x_train = np.empty((7000,fmsize),dtype=np.float32)

for i in range(7000):
    curr = manip.flattenmat(mats[i])
    x_train[i] = curr


lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=1e-4,
    decay_steps=5000,
    decay_rate=0.95,
    staircase=True)
optimizer = keras.optimizers.RMSprop(learning_rate=lr_schedule)

inputs = keras.Input(shape=(fmsize,))
dense = layers.Dense(500,activation='tanh')
x = dense(inputs)
x = layers.Dense(500,activation='tanh')(x)
outputs = layers.Dense(fmsize,activation='tanh')(x)
model = keras.Model(inputs=inputs,outputs=outputs,name='autoencoder')

print(model.summary())


model.compile(
    loss=keras.losses.MeanSquaredError(),
    optimizer=optimizer,
    metrics=['accuracy'],
)


history = model.fit(x_train,x_train,batch_size=150,epochs=100)

x_test = np.empty((1,fmsize),dtype=np.float32)
x_test[0] = manip.flattenmat(mats[9000])


prediction = model.predict(x_test)
print(prediction[0])
print(x_test[0])
