import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import pickle
import sys
sys.path.insert(1,'../')
from latmattools.manip import manip
np.set_printoptions(precision=2,threshold=sys.maxsize)

model = keras.Sequential()

Ls = 2
Lt = 2

fmsize = 2*(3*Lt*Ls**3)**2
msize = 2*3*Lt*Ls**3

mats = pickle.load(open('../examples/picklejar/allmats-m0.001-10000','rb'))
invmats = pickle.load(open('../examples/picklejar/allinvmats-m0.001-10000','rb'))


#x_train = np.empty((7000,msize,msize,1),dtype=np.float32)
x_train = np.empty((7000*3*Lt*Ls**3,fmsize+3*Lt*Ls**3),dtype=np.float32)

for i in range(7000):
    for j in range(3*Lt*Ls**3):
        vec = np.zeros(3*Lt*Ls**3,dtype=np.float32)
        vec[j] = 1.
        #matvec = mats[1000+i].dot(vec)
        #curr = manip.cmplx2real(mats[1000+i]) #manip.flattenmat(mats[1000+i])
        curr = manip.flattenmat(mats[1000+i])
        matandvec = np.concatenate([curr,vec])
        #print(matandvec)
        #print(matandvec.shape())

        #curr = manip.cmplx2realvec(matvec)
        #curr = curr.reshape((msize,msize,1))
        #minarg = np.argmin(curr)
        #temp = curr-curr[minarg]
        #maxarg = np.argmax(np.absolute(temp)) 
        #x_train[i] = temp/temp[maxarg]
        x_train[(3*Lt*Ls**3)*i+j] = matandvec


#print(x_train[0])
#print()
#print(x_train[1])

y_train = np.empty((7000*3*Lt*Ls**3,msize),dtype=np.float32)

maxs = []

for i in range(7000):
    for j in range(3*Lt*Ls**3):
        vec = np.zeros(3*Lt*Ls**3,dtype=np.float32)
        vec[j] = 1.
        matvec = invmats[1000+i].dot(vec)
    
        curr = manip.cmplx2realvec(matvec)
        maxval = np.amax(np.absolute(curr))
        y_train[(3*Lt*Ls**3)*i+j] = curr/maxval

    #curr = manip.flattenmat(invmats[1000+i])
    ##minarg = np.argmin(curr)
    ##temp = curr-curr[minarg]
    #maxarg = np.argmax(np.absolute(curr)) 
    #y_train[i] = curr/np.absolute(curr[maxarg])
    #maxs.append(np.absolute(curr[maxarg]))
    ##y_train[i] = curr

print('normalisation: ',np.mean(maxs),np.std(maxs))

lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=5e-4,
    decay_steps=1000000,
    decay_rate=0.96,
    staircase=True)
#optimizer = keras.optimizers.RMSprop(learning_rate=lr_schedule)
optimizer = keras.optimizers.RMSprop(lr=5e-4)

#inputs = keras.Input(shape=(msize,msize,1,))
inputs = keras.Input(shape=(fmsize+3*Lt*Ls**3,))
#dense = layers.Dense(1000,activation='tanh')
#x = dense(inputs)
x = layers.Dense(48,activation='tanh')(inputs)
#x = layers.Conv2D(32,3,activation='tanh',strides=2,padding="same")(inputs)
x = layers.Dense(48,activation='tanh')(x)
x = layers.Dense(48,activation='tanh')(x)
#x = layers.Flatten()(x)
outputs = layers.Dense(msize,activation='tanh')(x)
model = keras.Model(inputs=inputs,outputs=outputs,name='solver')

print(model.summary())

print(model.input_shape)
print(model.output_shape)


model.compile(
    loss=keras.losses.MeanSquaredError(),
    #loss=keras.losses.BinaryCrossentropy(),
    optimizer=optimizer,
    #optimizer=keras.optimizers.Adam(),
    metrics=['accuracy'],
)


history = model.fit(x_train,y_train,batch_size=200,epochs=100)

#x_test = np.empty((1,msize,msize,1),dtype=np.float32)
#cmplxmat = manip.cmplx2real(mats[9000])
#cmplxmat = cmplxmat.resize((msize,msize,1))
#x_test[0] = cmplxmat


#x_test = np.empty((1,fmsize),dtype=np.float32)
#cmplxmat = manip.flattenmat(mats[9000])
##cmplxmat = cmplxmat.resize((msize,msize,1))
#x_test[0] = cmplxmat


#y_test = np.empty((1,fmsize),dtype=np.float32)
#curr = manip.flattenmat(invmats[9000])
#maxarg = np.argmax(np.absolute(curr))
#y_test[0] = curr/np.absolute(curr[maxarg])

#prediction = model.predict(x_test)
#print(prediction[0])
#print(y_test[0])

#pickle.dump(prediction[0],open('./picklejar/predictedinvnew','wb'))
