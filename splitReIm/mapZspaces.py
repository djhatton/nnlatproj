import numpy as np
import pickle
import argparse

from pprint import pprint as pp

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras import initializers

import sys
for insertPath in ['../', '../examples', 'Utils']:
    sys.path.insert(1, insertPath)

from normArrays import ArrayNormaliser


def initArch(nbDimms, actFct, nbNeur, λVal=0.0, fitHandle='ZSpaceFit'):
    '''
        Initialise the linear architecture.
    '''

    # Initialise the Neural network
    inputLayer = keras.Input(shape=(nbDimms,))
    denseLayer = layers.Dense(nbNeur, activation=actFct, activity_regularizer=regularizers.l2(λVal))
    a1 = denseLayer(inputLayer)

    outLayer = layers.Dense(2, activation=actFct, activity_regularizer=regularizers.l2(λVal))(a1)
    model = keras.Model(inputs=inputLayer, outputs=outLayer, name=fitHandle)

    #  Plot out the neural net and compile it
    # keras.utils.plot_model(model, fitHandle + ".png", show_shapes=True)
    # model.compile(loss='mean_squared_error', optimizer='adam')
    model.compile(loss=tf.keras.losses.KLDivergence(), optimizer='adam')
    model.summary()

    return model


def normData(matArray, normType='UnitVec'):
    '''
        If normType is:
            - 'SetMeanStd': normalise data via Z = (X - μ) / σ.
            - 'UnitVec': normalise each matrix as M / det(M).
    '''
    print(f'Normalising as {normType}!')
    if normType == 'SetMeanStd':
        return (matArray - np.mean(matArray)) / np.std(matArray)
    elif normType == 'UnitVec':

        nbMatrices = matArray.shape[0]
        for matNb in range(nbMatrices):

            unitNorm = np.sqrt(sum([comp**2 for comp in matArray[matNb]]))
            matArray[matNb] = matArray[matNb] / unitNorm

        return matArray
    else:
        # print(f'{normType} is not a known normalisation! Returning original array.')
        warnings.warn(f'{normType} is not a known normalisation! Returning original array.')
        return matArray


def initParser():
    '''
    '''
    parser = argparse.ArgumentParser(description='Process the inputs for the latent Z space mapping.')
    parser.add_argument('Re//Im', help='Input Re to train the real part, Im to train the imaginary part')

    parser.add_argument("-nE", '--nbOfEpochs', help='Specify number of training epochs.',
                        type=int, default=1000)
    parser.add_argument("-mB", '--mBatch', help='Specify minibatch size.',
                        type=int, default=128)
    parser.add_argument("-nNN", '--nbOfNeurons', help='Specify number of neurons for flat architecture with 1 layer.', type=int, default=100)
    parser.add_argument('--normData', help='Set flag to normalise data as either UnitVec or SetMeanStd', type=str, default='')
    parser.add_argument('--validSplit', help='Specify the validation split, default to 0.8.', type=float,
     default=0.2)
    argsPars = parser.parse_args()
    trainCard = vars(argsPars)

    return trainCard


if __name__ == '__main__':
    '''
        Main calls for the Z space fits.
    '''
    trainCard = initParser()
    commRoot_Mat, commRoot_Inv = 'LatentZspaces/matArray_Zspace_mat-', 'LatentZspaces/invMatArray_Zspace_invMat-'
    vaeStr = '/VAE_Info.p'

    # Matrix Z space declarations
    zSpace_ReM, zSpace_ImM = 'linArch_N300_relu_tanh_Z2_Re', 'linArch_N300_relu_tanh_Z2_Im'

    # Inverse Matrix Z space declarations
    zSpace_ReInvM, zSpace_ImInvM = 'linArch_N300_relu_tanh_Z2_Re', 'linArch_N1000_relu_tanh_Z2_Im'

    zSpaceDicts = {'ReM': commRoot_Mat + zSpace_ReM + vaeStr,
                   'ImM': commRoot_Mat + zSpace_ImM + vaeStr,
                   'ReInvM': commRoot_Inv + zSpace_ReInvM + vaeStr,
                   'ImInvM': commRoot_Inv + zSpace_ImInvM + vaeStr}

    for zSpaceType in zSpaceDicts.keys():
        with open(zSpaceDicts[zSpaceType], 'rb') as pZIn:
            zSpace_Dict = pickle.load(pZIn)
        zSpaceDicts[zSpaceType] = zSpace_Dict

    # NN declarations
    zSpace = np.concatenate((zSpaceDicts['ReM']['Zspace'], zSpaceDicts['ImM']['Zspace']), axis=1)
    inputShape = zSpace.shape[1]
    fitFct = 'relu'
    nbNeur = trainCard['nbOfNeurons']
    mBatch = trainCard['mBatch']
    nbEpochs = trainCard['nbOfEpochs']

    fitModel = initArch(inputShape, fitFct, nbNeur)

    # Normalise data
    zNormer = ArrayNormaliser(zSpace, trainCard['normData'])
    zSpaceDict = zNormer.normData(zSpace)
    zSpace = zSpaceDict['NormArray']

    # Fit and save the Z space mappings and the normalisation instance
    fitHandle = trainCard['Re//Im'] + 'InvM'
    fitHistory = fitModel.fit(zSpace, zSpaceDicts[fitHandle]['Zspace'], batch_size=mBatch,
                              epochs=nbEpochs, validation_split=0.2, verbose=1)
    fitModel.save('zSpaceMaps/latZfit_4-2_' + trainCard['Re//Im'])
    with open('zSpaceMaps/latZfit_4-2_' + trainCard['Re//Im'] + '/Znorm.p', 'wb') as pcklOut:
        pickle.dump(zNormer, pcklOut)
