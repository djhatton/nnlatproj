import numpy as np
import pickle
import sys
import matplotlib.pyplot as plt
import tensorflow as tf
import argparse
import warnings


for insertPath in ['../', '../examples', 'Utils']:
    sys.path.insert(1, insertPath)


from normArrays import ArrayNormaliser
from latmattools.manip import manip
np.set_printoptions(precision=2, threshold=sys.maxsize)


from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras import initializers


#  FNULL declaration to supress cmd line output ####
import os
import subprocess
FNULL = open(os.devnull, 'w')

#  Sampling layer
class Sampling(layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon


class VAE(keras.Model):
    def __init__(self, encoder, decoder, **kwargs):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder

    def train_step(self, data):
        if isinstance(data, tuple):
            data = data[0]
        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            reconstruction_loss = tf.reduce_mean(
                keras.losses.binary_crossentropy(data, reconstruction)
            )
            reconstruction_loss *= 28 * 28
            kl_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
            kl_loss = tf.reduce_mean(kl_loss)
            kl_loss *= -0.5
            total_loss = reconstruction_loss + kl_loss
        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        return {
            "loss": total_loss,
            "reconstruction_loss": reconstruction_loss,
            "kl_loss": kl_loss,
        }


def plotZspace(encoder, data, figHandle, labels='IndxPos', runDict={}):
    '''
        Given uncompressed high dimensional data the function uses the encoder prediction for the Z space, which is
        then plotted in a 2D array. It is then saved into a pickle file in the latent Z space directory.
    '''
    nbPoints = data.shape[0]
    z_mean, _, _ = encoder.predict(data)
    plt.figure(figsize=(12, 10))

    saveHandle = figHandle + '_' + runDict['VAE'] + '/'
    subprocess.call('mkdir LatentZspaces/' + saveHandle, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)

    # Use the matrix number as the color ID to correlate between matrices and their inverse
    if labels == 'IndxPos':
        labels = np.array([i for i in range(nbPoints)])
    plt.scatter(z_mean[:, 0], z_mean[:, 1], c=labels, cmap='viridis')
    plt.colorbar()
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig('LatentZspaces/' + saveHandle + 'LatZ.pdf')
    plt.show()

    # Export the Z space with the run dictionary
    pckDict = {'runDict': runDict, 'Zspace': z_mean}
    with open('LatentZspaces/' + saveHandle + 'VAE_Info.p', 'wb') as pcklOut:
        pickle.dump(pckDict, pcklOut)


def initArch(matArray, archType):
    '''
        Initialises and passe the training set and the architecture type.
    '''
    if archType == 'Linear':
        return linearArch(matArray)
    elif archType == 'Conv_V1':
        return convArch_v1(matArray)
    elif archType == 'Conv_V2':
        return convArch_v2(matArray)


def linearArch(matArray, nbNeur=500, outNeuron='tanh', layerAct='relu', latent_dim=2, auxHandle=None):
    '''
        Returns the linear architecture VAE.
    '''
    nbMatrices = matArray.shape[0]
    trainMats = []
    for i in range(nbMatrices):
        trainMats.append(matArray[i].flatten())
    trainMats = np.array(trainMats)
    n_dimm = trainMats.shape[1]

    # Save models
    vaeID = f'{auxHandle[0]}-linArch_N{nbNeur}_{layerAct}_{outNeuron}_Z{latent_dim}_{auxHandle[1]}'
    subprocess.call('mkdir VAEs/', shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    dirToMake = 'VAEs/' + vaeID + '/'
    subprocess.call('mkdir ' + dirToMake, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)

    subDirs = ['Encoder/', 'Decoder/']
    for subDir in subDirs:
        subprocess.call('mkdir ' + dirToMake + subDir, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)

    # Rectified Linear Unit architecture
    λVal = 0.0 * 1e-5

    #  Build encoder
    encoder_inputs = keras.Input(shape=(n_dimm,))
    x = layers.Dense(nbNeur, activation=layerAct, activity_regularizer=regularizers.l2(λVal))(encoder_inputs)
    z_mean = layers.Dense(latent_dim, name="z_mean")(x)
    z_log_var = layers.Dense(latent_dim, name="z_log_var")(x)
    z = Sampling()([z_mean, z_log_var])
    encoder = keras.Model(encoder_inputs, [z_mean, z_log_var, z], name="encoder")
    encoder.summary()

    #  Build decoder
    latent_inputs = keras.Input(shape=(latent_dim,))
    x = layers.Dense(nbNeur, activation="relu")(latent_inputs)
    decoder_outputs = layers.Dense(n_dimm, activation=outNeuron, activity_regularizer=regularizers.l2(λVal))(x)
    decoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
    decoder.summary()

    if True:
        keras.utils.plot_model(encoder, dirToMake + "Encoder/encoderModel.png", show_shapes=True)
        keras.utils.plot_model(decoder, dirToMake + "Decoder/decoderModel.png", show_shapes=True)
        encoder.save(dirToMake + subDirs[0])
        decoder.save(dirToMake + subDirs[1])

    return encoder, decoder, trainMats, vaeID


def convArch_v1(matArray, kernSize=3, outNeuron='sigmoid', layerAct='relu', latent_dim=2):
    '''
        Convolutional architecture V1, with 64 -> 128 -> 32.
    '''
    matSize = matArray.shape[1]

    # Build encoder
    encoder_inputs = keras.Input(shape=(matSize, matSize, 1))
    x = layers.Conv2D(64, kernSize, activation=layerAct, strides=2, padding="same")(encoder_inputs)
    x = layers.Conv2D(128, kernSize, activation=layerAct, strides=2, padding="same")(x)
    x = layers.Flatten()(x)
    x = layers.Dense(32, activation=layerAct)(x)
    z_mean = layers.Dense(latent_dim, name="z_mean")(x)
    z_log_var = layers.Dense(latent_dim, name="z_log_var")(x)
    z = Sampling()([z_mean, z_log_var])
    encoder = keras.Model(encoder_inputs, [z_mean, z_log_var, z], name="encoder")
    encoder.summary()
    keras.utils.plot_model(encoder, "encoderModel.png", show_shapes=True)

    #  Build decoder
    latent_inputs = keras.Input(shape=(latent_dim,))
    x = layers.Dense(12 * 12 * 128, activation=layerAct)(latent_inputs)
    x = layers.Reshape((12, 12, 128))(x)
    x = layers.Conv2DTranspose(128, kernSize, activation=layerAct, strides=2, padding="same")(x)
    x = layers.Conv2DTranspose(64, kernSize, activation=layerAct, strides=2, padding="same")(x)
    decoder_outputs = layers.Conv2DTranspose(1, kernSize, activation=outNeuron, padding="same")(x)
    decoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
    decoder.summary()
    keras.utils.plot_model(decoder, "decoderModel.png", show_shapes=True)
    trainMats = np.expand_dims(matArray, -1)  # .astype("float32")

    return encoder, decoder, trainMats


def convArch_v2(matArray, kernSize=3, outNeuron='sigmoid', layerAct='relu', latent_dim=2):
    '''
        Convolutional architecture V1, with 32 -> 64 -> 16.
    '''
    matSize = matArray.shape[1]

    # Build encoder
    encoder_inputs = keras.Input(shape=(matSize, matSize, 1))
    x = layers.Conv2D(32, kernSize, activation=layerAct, strides=2, padding="same")(encoder_inputs)
    x = layers.Conv2D(64, kernSize, activation=layerAct, strides=2, padding="same")(x)
    x = layers.Flatten()(x)
    x = layers.Dense(16, activation=layerAct)(x)
    z_mean = layers.Dense(latent_dim, name="z_mean")(x)
    z_log_var = layers.Dense(latent_dim, name="z_log_var")(x)
    z = Sampling()([z_mean, z_log_var])
    encoder = keras.Model(encoder_inputs, [z_mean, z_log_var, z], name="encoder")
    encoder.summary()
    keras.utils.plot_model(encoder, "encoderModel.png", show_shapes=True)

    #  Build decoder
    latent_inputs = keras.Input(shape=(latent_dim,))
    x = layers.Dense(12 * 12 * 64, activation=layerAct)(latent_inputs)
    x = layers.Reshape((12, 12, 64))(x)
    x = layers.Conv2DTranspose(64, kernSize, activation=layerAct, strides=2, padding="same")(x)
    x = layers.Conv2DTranspose(32, kernSize, activation=layerAct, strides=2, padding="same")(x)
    decoder_outputs = layers.Conv2DTranspose(1, kernSize, activation=outNeuron, padding="same")(x)
    decoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
    decoder.summary()
    keras.utils.plot_model(decoder, "decoderModel.png", show_shapes=True)
    trainMats = np.expand_dims(matArray, -1)  # .astype("float32")

    return encoder, decoder, trainMats


# def normData(matArray, normType='UnitVec'):
#     '''
#         If normType is:
#             - 'SetMeanStd': normalise data via Z = (X - μ) / σ.
#             - 'UnitVec': normalise each matrix as M / det(M).
#     '''
#     print(f'Normalising as {normType}!')
#     if normType == 'SetMeanStd':
#         return (matArray - np.mean(matArray)) / np.std(matArray)
#     elif normType == 'UnitVec':
#
#         nbMatrices = matArray.shape[0]
#         for matNb in range(nbMatrices):
#             unitNorm = np.linalg.det(matArray[matNb])
#             matArray[matNb] = matArray[matNb] / unitNorm
#
#         return matArray
#     else:
#         # print(f'{normType} is not a known normalisation! Returning original array.')
#         warnings.warn(f'{normType} is not a known normalisation! Returning original array.')
#         return matArray


def splitReIm(matArray):
    '''
        Given a matrix array the funciton returns two arrays, one with the real parts and the other with
        imaginary parts.
    '''
    return matArray.real, matArray.imag


if __name__ == '__main__':
    # #################################     Parser Arguments      ###########################################
    parser = argparse.ArgumentParser(description='Process the inputs for the plotting function')
    parser.add_argument('mat//invMat', help='Input mat to train the matrix array, invMat for the inverse.')
    parser.add_argument('Re//Im', help='Input Re to train the real part, Im to train the imaginary part')

    parser.add_argument("-nE", '--nbOfEpochs', help='Specify number of training epochs.',
                        type=int, default=50)
    parser.add_argument("-mB", '--mBatch', help='Specify minibatch size.',
                        type=int, default=128)
    parser.add_argument("-nNN", '--nbOfNeurons', help='Specify number of neurons for flat architecture with 1 layer.', type=int, default=300)
    parser.add_argument('--normData', help='Set flag to normalise data as either UnitVec or SetMeanStd', type=str, default='')
    parser.add_argument('--trainSplit', help='Specify the training split, default to 0.8.', type=float, default=0.8)
    argsPars = parser.parse_args()
    trainCard = vars(argsPars)

    # Set the random seed and load the data
    rndSeed = 0
    np.random.seed(rndSeed)
    matArray = pickle.load(open('../examples/picklejar/allmats-10000', 'rb'))
    invMatArray = pickle.load(open('../examples/picklejar/allinvmats-10000', 'rb'))
    matDict = {'matArray': matArray, 'invMatArray': invMatArray}

    #   Initialise the parameters
    nbEpochs = trainCard['nbOfEpochs']
    mBatch = trainCard['mBatch']

    nbNeur = trainCard['nbOfNeurons']
    runSet = trainCard['mat//invMat'] + 'Array'
    archType = 'Linear'

    # Set the training split and select a random subset from the data.
    trainSplit = trainCard['trainSplit']
    boolMask = np.array([True if np.random.uniform() < trainSplit else False
                         for _ in range(matDict[runSet].shape[0])
                         ])
    trainArray, testArray = matDict[runSet][boolMask], matDict[runSet][np.logical_not(boolMask)]

    trainArray_Re, trainArray_Im = splitReIm(trainArray)
    trainDict = {'Re': trainArray_Re, 'Im': trainArray_Im}

    # Cannot use unit vector normalisation for the inverse matrix VAE.
    # if trainCard['normData'] != '':
    if trainCard['normData'] == 'UnitVec' and trainCard['mat//invMat'] == 'invMat':
        raise KeyError('Cannot use unit vector normalisation for the inverse matrix VAE!')
    else:
        matNormer = ArrayNormaliser(trainDict[trainCard['Re//Im']], trainCard['normData'])
        trainArray = matNormer.normData(trainDict[trainCard['Re//Im']])

    encoder, decoder, trainMats, vaeID = linearArch(trainArray['NormArray'], nbNeur=nbNeur,
        auxHandle=[trainCard['mat//invMat'], trainCard['Re//Im']])

    #  Compile and train VAEs
    vae = VAE(encoder, decoder)
    vae.compile(optimizer=keras.optimizers.Adam())
    vaeHist = vae.fit(trainMats, epochs=nbEpochs, batch_size=mBatch)
    encoder.save('VAEs/' + vaeID + '/' + 'Encoder/')
    decoder.save('VAEs/' + vaeID + '/' + 'Decoder/')

    # Plot and save the Z space for the training set
    runDict = {'Arch': archType, 'Epochs': nbEpochs, 'mBatch': mBatch,
               'VAE': vaeID, 'Epochs': nbEpochs,
               'Train': runSet, 'TrainSplit': trainSplit, 'RndSeed': rndSeed,
               'FitLosses': vaeHist.history, 'NormType': trainCard['normData'],
               'NormInstance': matNormer
               }
    plotZspace(encoder, trainMats, runSet + '_Zspace', runDict=runDict)
