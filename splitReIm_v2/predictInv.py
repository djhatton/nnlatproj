import numpy as np
import pickle
import argparse

from pprint import pprint as pp

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras import initializers

import sys
for insertPath in ['../', '../examples', 'Utils']:
    sys.path.insert(1, insertPath)

from normArrays import ArrayNormaliser


def loadTestMats():
    '''
        Loads the test matrices according to the train split and returns the flattened array/
    '''
    # Set the random seed and split the matrix set
    np.random.seed(0)
    matArray = pickle.load(open('../examples/picklejar/allmats-10000', 'rb'))
    matDict = {'matArray': matArray
               # , 'invMatArray': invMatArray
               }
    trainSplit = 0.8  # zSpace['TrainSplit']
    boolMask = np.array([True if np.random.uniform() < trainSplit else False
                         for _ in range(matDict['matArray'].shape[0])
                         ])
    testArray = matDict['matArray'][np.logical_not(boolMask)]

    return testArray.real, testArray.imag, testArray


def loadInvTestMats():
    '''
        Loads the test matrices according to the train split and returns the flattened array/
    '''
    # Set the random seed and split the matrix set
    np.random.seed(0)
    matArray = pickle.load(open('../examples/picklejar/allinvmats-10000', 'rb'))
    matDict = {'matArray': matArray
               # , 'invMatArray': invMatArray
               }
    trainSplit = 0.8  # zSpace['TrainSplit']
    boolMask = np.array([True if np.random.uniform() < trainSplit else False
                         for _ in range(matDict['matArray'].shape[0])
                         ])
    testArray = matDict['matArray'][np.logical_not(boolMask)]

    return testArray


def flattenMats(testArray):
    # Flatten the matrices for the linear architecture
    nbMatrices = testArray.shape[0]
    testMats = []
    for i in range(nbMatrices):
        testMats.append(testArray[i].flatten())
    testMats = np.array(testMats)
    return testMats


def loadNormer(netStr):
    '''
        Load up the normalisation instance associated with the netStr.
    '''
    if 'zSpaceMaps' not in netStr:
        with open(netStr + '/VAE_Info.p', 'rb') as pcklIn:
            netAttrs = pickle.load(pcklIn)

        return netAttrs['runDict']['NormInstance']
    else:
        with open(netStr + '/Znorm.p', 'rb') as pcklIn:
            zNormInst = pickle.load(pcklIn)
        return zNormInst


def getMatChiSq(matM, reMinv, imMinv):
    '''
        Returns the chi^2 measure for the real and the imaginary conditions
    '''
    matSizes = matM.shape[1]
    reM, imM = matM.real, matM.imag
    reChi2Mat = np.dot(reM, reMinv) - np.dot(imM, imMinv)
    imChi2Mat = np.dot(reM, imMinv) + np.dot(reMinv, imM)

    return {'ReChi2': np.linalg.det(reChi2Mat - np.identity(matSizes)),
            'ImChi2': np.linalg.det(imChi2Mat)}


def insertInvZ(zInv_Re, zInv_Im, zInvTrue, invMatDecoder_Re, invMatDecoder_Im, invMatRe_Normer, invMatIm_Normer, zUnit=0.05, nbGen=1000, sampling='Uniform'):
    '''
        Generates a number of nbGen inverse matrices drawn from the inverse latent space to select the minimum
        Chi2 value.
    '''
    matDim = 48

    zInvRe_Array = zInv_Re + np.random.uniform(low=0.0, high=zUnit, size=(nbGen, zInv_Re.shape[0]))
    zInvIm_Array = zInv_Im + np.random.uniform(low=0.0, high=zUnit, size=(nbGen, zInv_Re.shape[0]))

    invMatRe = invMatDecoder_Re.predict(zInvRe_Array).reshape((nbGen, matDim, matDim))
    invMatRe = invMatRe_Normer.invNormData(invMatRe, '')

    invMatIm = invMatDecoder_Im.predict(zInvIm_Array).reshape((nbGen, matDim, matDim))
    invMatIm = invMatIm_Normer.invNormData(invMatIm, '')

    chi2List = []
    for matNb in range(nbGen):
        chiSqDict = getMatChiSq(zInvTrue, invMatRe[matNb], invMatIm[matNb])
        # print(chiSqDict)
        chi2List.append(chiSqDict['ReChi2']**2 + chiSqDict['ImChi2']**2)

    # chiSqDict = getMatChiSq(zInvTrue, zInv_Re, zInv_Im)
    # chi2List.append(chiSqDict['ReChi2']**2 + chiSqDict['ImChi2']**2)

    # zeroChi2Dict = getMatChiSq(zInvTrue, np.ones((matDim, matDim)), np.ones((matDim, matDim)))
    # zeroChi2 = zeroChi2Dict['ReChi2']**2 + zeroChi2Dict['ImChi2']**2
    # if min(chi2List) < zeroChi2:

    minChi2 = min(chi2List)
    minIdx = chi2List.index(min(chi2List))

    # print(())
    # print('Beat it :', min(chi2List), '-----', chi2List.index(min(chi2List)))
    return {'Mat': invMatRe[minIdx] + 1j * invMatIm[matNb], 'Chi2': minChi2}


if __name__ == '__main__':
    # Load up the test Array
    testArray_Re, testArray_Im, testArray = loadTestMats()
    nbMats, matDim = testArray_Re.shape[0],  testArray_Re.shape[1]

    # Load up the encoders
    matVae_Re, matVae_Im = 'mat-linArch_N300_relu_tanh_Z2_Re', 'mat-linArch_N300_relu_tanh_Z2_Im'
    matEncoder_Re = keras.models.load_model('VAEs/' + matVae_Re + '/Encoder')
    matEncoder_Im = keras.models.load_model('VAEs/' + matVae_Im + '/Encoder')

    # Load up Normalisations and propagate
    matRe_Normer = loadNormer('LatentZspaces/matArray_Zspace_' + matVae_Re)
    reMats = flattenMats(matRe_Normer.normData(testArray_Re)['NormArray'])
    z_Re, _, _ = matEncoder_Re.predict(reMats)

    matIm_Normer = loadNormer('LatentZspaces/matArray_Zspace_' + matVae_Im)
    imMats = flattenMats(matIm_Normer.normData(testArray_Im)['NormArray'])
    z_Im, _, _ = matEncoder_Im.predict(reMats)

    zSpace = np.concatenate((z_Re, z_Im), axis=1)

    # Load up Mapping between latent spaces
    zReStr, zImStr = 'zSpaceMaps/latZfit_4-2_Re', 'zSpaceMaps/latZfit_4-2_Im'
    zMap_Re = keras.models.load_model(zReStr)
    zMap_Im = keras.models.load_model(zImStr)

    zRe_Normer, zIm_Normer = loadNormer(zReStr), loadNormer(zImStr)
    zSpace_ReNorm = zRe_Normer.normData(zSpace)['NormArray']
    zSpace_ImNorm = zIm_Normer.normData(zSpace)['NormArray']
    zInv_Re = zMap_Re.predict(zSpace_ReNorm)
    zInv_Im = zMap_Im.predict(zSpace_ImNorm)

    # Load up the decoders
    invMatVae_Re, invMatVae_Im = 'invMat-linArch_N300_relu_tanh_Z2_Re', 'invMat-linArch_N1000_relu_tanh_Z2_Im'
    invMatDecoder_Re = keras.models.load_model('VAEs/' + invMatVae_Re + '/Decoder')
    invMatDecoder_Im = keras.models.load_model('VAEs/' + invMatVae_Im + '/Decoder')

    # invMatRe = invMatDecoder_Re.predict(zInv_Re).reshape((nbMats, matDim, matDim))
    # invMatIm = invMatDecoder_Im.predict(zInv_Im).reshape((nbMats, matDim, matDim))

    invMatRe_Normer = loadNormer('LatentZspaces/invMatArray_Zspace_' + invMatVae_Re)
    # invMatRe = invMatRe_Normer.invNormData(invMatRe, '')
    invMatIm_Normer = loadNormer('LatentZspaces/invMatArray_Zspace_' + invMatVae_Im)
    # invMatIm = invMatIm_Normer.invNormData(invMatIm, '')

    predDict = {}
    for n in range(nbMats):
        pred_n = insertInvZ(zInv_Re[n], zInv_Im[n], testArray[n], invMatDecoder_Re, invMatDecoder_Im, invMatRe_Normer,
                              invMatIm_Normer)
        predDict[str(n)] = pred_n

    with open('Predictions/predInvMats.p', 'wb') as pcklOut:
        pickle.dump(predDict, pcklOut)
