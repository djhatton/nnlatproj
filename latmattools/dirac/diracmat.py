import numpy as np
from numba import jit

@jit(nopython=True)
def vector_from_index(index):
    comp01 = index%2
    newindex = index - comp01
    comp02 = (newindex%4)/2
    newindex -= comp02*2
    comp03 = (newindex%8)/4
    comp04 = (newindex - comp03*4)/8
    res = np.zeros(4,dtype=np.cdouble)
    res[0] = comp01
    res[1] = comp02
    res[2] = comp03
    res[3] = comp04
    return res

def alpha(mu,r):
    rvec = vector_from_index(r)
    exponent = 0.
    for i in range(4):
        if i<mu:
            exponent += rvec[i]
    return (-1)**exponent

def make_diracmat(gfield,Ls,Lt,m):

    print('Caution: make_diracmmat has not been fully checked for generality')
    vol = Lt*Ls**3
    mat = np.zeros((3*vol,3*vol),dtype=np.cdouble)

    for i in range(vol):
        Us = np.empty((4,3,3),dtype=np.cdouble)
        for a in range(3):
            for b in range(3):
                Us[0][a][b] = gfield[2*9*4*i + 3*a+b] + 1j*gfield[2*9*4*i + 3*a+b + 1]
                Us[1][a][b] = gfield[2*9*4*i + 3*a+b + 18] + 1j*gfield[2*9*4*i + 3*a+b + 18 + 1]
                Us[2][a][b] = gfield[2*9*4*i + 3*a+b + 36] + 1j*gfield[2*9*4*i + 3*a+b + 36 + 1]
                Us[3][a][b] = gfield[2*9*4*i + 3*a+b + 54] + 1j*gfield[2*9*4*i + 3*a+b + 54 + 1]
        for j in range(vol):
            for a in range(3):
                for b in range(3):
                    if i==j:
                        mat[3*i+a][3*j+b] += 2*m
                    elif j==(i+1)%(vol):
                        mat[3*i+a][3*j+b] += alpha(0,i)*Us[0][a][b]
                    elif j==(i+Ls)%(vol):
                        mat[3*i+a][3*j+b] += alpha(1,i)*Us[1][a][b]
                    elif j==(i+Ls**2)%(vol):
                        mat[3*i+a][3*j+b] += alpha(2,i)*Us[2][a][b]
                    elif j==(i+Ls**3)%(vol):
                        mat[3*i+a][3*j+b] += alpha(3,i)*Us[3][a][b]

        return mat
