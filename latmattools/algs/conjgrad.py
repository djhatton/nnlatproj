import numpy as np
import matplotlib.pyplot as plt
import copy

class ConjGrad:
    def __init__(self, dimension, source, intol=1e-6, inmaxiters=np.inf):
        self.tol = intol
        self.maxiters = inmaxiters
        self.dim = dimension
        self.niters = 0
        self.b = source
        self.initdist = np.zeros(self.dim,dtype=np.cdouble)
        self.x0 = np.zeros(self.dim,dtype=np.cdouble)
        self.A = np.identity(self.dim,dtype=np.cdouble)

    def setA(self, A):
        self.A = A

    def setx0(self, x0):
        self.x0 = x0

    def resetx0(self):
        self.x0 = np.zeros(self.dim,dtype=np.cdouble)

    def __call__(self):   
        At = np.conj(np.transpose(self.A))
        r0 = np.subtract(self.b,self.A.dot(self.x0))
        rt0 = np.subtract(self.b,At.dot(self.x0))
        self.initdist = np.dot(np.conj(rt0),rt0)
        x0 = copy.deepcopy(self.x0)
        p0 = r0
        pt0 = rt0
        k = 0
        while(k<self.maxiters):
            alpha = np.dot(np.conj(rt0),r0)/np.dot(np.conj(pt0),self.A.dot(p0))
            x1 = np.add(x0,alpha*p0)
            r1 = np.subtract(r0,alpha*self.A.dot(p0))
            rt1 = np.subtract(rt0,np.conj(alpha)*At.dot(pt0))
            if np.sqrt(np.dot(np.conj(r1),r1).real) < self.tol:
                break
            beta = np.dot(np.conj(rt1),r1)/np.dot(np.conj(rt0),r0)
            k += 1
            p0 = np.add(r1,beta*p0)
            pt0 = np.add(rt1,np.conj(beta)*pt0)
            x0 = x1
            r0 = r1
            rt0 = rt1
        self.niters = k
        return x1

class CGTest:
    def __init__(self,_preds,_mats,_source,_diffplotname='./cgtests_diff.pdf',_distplotname='./cgtests_distances.pdf'):
        self.preds = _preds
        self.mats = _mats
        self.source = _source
        self.diffplotname = _diffplotname 
        self.distplotname = _distplotname 
        self.samplesize = np.shape(self.preds)[0]
        self.matsize =  np.shape(self.preds)[1]
        self.CG = ConjGrad(self.matsize,self.source)
        self.itercounts = []
        self.initdists = []
        self.itercountspred = []
        self.initdistspred = []

    def __call__(self):
        for mat,pred in zip(self.mats,self.preds):
            self.CG.setA(mat)
            self.CG()
            self.itercounts.append(self.CG.niters)
            self.initdists.append(self.CG.initdist)
            x0 = pred.dot(self.source)
            self.CG.setx0(x0)
            self.CG()
            self.itercountspred.append(self.CG.niters)
            self.initdistspred.append(self.CG.initdist)
            self.CG.resetx0()
        return

    def make_plots(self):
        
        plt.rc('font', **{'family': 'serif', 'serif': ['Computer Modern'],'size':12})
        plt.rc('text', usetex=True)
        plt.rc('axes', linewidth=0.5)
        #plt.rcParams['savefig.dpi'] = 200
        plt.figure(figsize=((4.5,4.5/1.618)))
        plt.gca().tick_params(right=True,top=True,direction='in')

        plt.plot(range(self.samplesize),[p1-p2 for p1,p2 in zip(self.itercounts,self.itercountspred)],color='b')
        plt.ylabel('$\mathrm{iters}_0 - \mathrm{iters}_{\mathrm{pred}}$')
        plt.tight_layout()
        print('saving plot to ',self.diffplotname)
        plt.savefig(self.diffplotname)
        plt.close()

        plt.rc('font', **{'family': 'serif', 'serif': ['Computer Modern'],'size':12})
        plt.rc('text', usetex=True)
        plt.rc('axes', linewidth=0.5)
        #plt.rcParams['savefig.dpi'] = 200
        plt.figure(figsize=((4.5,4.5/1.618)))
        plt.gca().tick_params(right=True,top=True,direction='in')

        plt.plot(range(self.samplesize),[np.sqrt(p.real) for p in self.initdistspred],color='b',label='pred')
        plt.plot(range(self.samplesize),[np.sqrt(p.real) for p in self.initdists],color='r',label='$x_0 = 0$')
        plt.ylabel('$\\vert b-Ax_0 \\vert$')
        handles,labels = plt.gca().get_legend_handles_labels()
        plt.legend(handles=handles,labels=labels,frameon=False)
        plt.tight_layout()
        print('saving plot to ',self.distplotname)
        plt.savefig(self.distplotname)
        plt.close()

        return
