import struct
import numpy as np

def readgauge(gfile,Ls,Lt):
"""
reads MILC gauge configuration files
"""

    infile = open(gfile,'rb')

    vol = Lt*Ls**3

    # the MILC configuration files have a 96 byte header
    # factor of 2 for real and imaginary parts
    # factor of 4 for space-time directions
    # factor of 9 for colour
    total = 24 + 2*4*9*vol

    count = 0

    fieldarray = []

    for i in range(total):
        # read in 4 byte chunks
        # saved as 32-bit floats hence 4 bytes
        chunk = (struct.unpack('f',infile.read(4)))
        # skip the header
        if i > 23:
            fieldarray.append(chunk[0]) 
            count += 1

    return np.array(fieldarray)

