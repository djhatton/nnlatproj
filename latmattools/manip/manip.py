import numpy as np
from numba import jit

@jit(nopython=True)
def flattenmat(mat):
    """
    flatten matrix
    assumes square matrix
    """
    assert len(np.shape(mat))==2,'matrix should have dimension 2'
    assert np.shape(mat)[0]==np.shape(mat)[1],'matrix should be square'
    msize = np.shape(mat)[0]
    flatmat = np.zeros(2*msize**2,dtype=np.float32)
    for a in range(msize):
        for b in range(msize):
            flatmat[2*(msize*a+b)] = mat[a][b].real
            flatmat[2*(msize*a+b)+1] = mat[a][b].imag
    return flatmat

@jit(nopython=True)
def unflattenmat(flatmat):
    """
    rebuild a matrix from a flattened one
    """
    assert len(np.shape(flatmat))==1,"flat matrix should be an array of dimension 1"
    msize = int(np.sqrt(len(flatmat)/2))
    mat = np.zeros((msize,msize),dtype=np.cdouble)
    for a in range(msize):
        for b in range(msize):
            mat[a][b] = flatmat[2*(msize*a+b)] + 1j*flatmat[2*(msize*a+b)+1]
    return mat

@jit(nopython=True)
def cmplx2real(mat):
    """
    assumes square matrix
    """
    assert len(np.shape(mat))==2,'matrix should have dimension 2'
    assert np.shape(mat)[0]==np.shape(mat)[1],'matrix should be square'
    msize = np.shape(mat)[0]
    flatmat = np.zeros((2*msize,2*msize),dtype=np.float32)
    for a in range(msize):
        for b in range(msize):
            flatmat[2*a][2*b] = mat[a][b].real
            flatmat[2*a+1][2*b+1] = mat[a][b].imag
    return flatmat

@jit(nopython=True)
def cmplx2realvec(vec):
    assert len(np.shape(vec))==1,'vector should have dimension 1'
    vsize = np.shape(vec)[0]
    flatvec = np.zeros(2*vsize,dtype=np.float32)
    for a in range(vsize):
        flatvec[2*a] = vec[a].real
        flatvec[2*a+1] = vec[a].imag
    return flatvec

@jit(nopython=True)
def thinflatmat(flatmat):
    """
    remove zeros from flatmat
    """
    assert len(np.shape(flatmat))==1,"flat matrix should be an array of dimension 1"
    fmsize = np.shape(flatmat)[0]
    nz = 0
    for p in flatmat:
        if p==0:
          nz += 1
    res = np.zeros(fmsize-nz,dtype=np.float32)
    i = 0
    for p in flatmat:
        if p != 0:
            res[i] = p
            i += 1
    return res
